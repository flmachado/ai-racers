import controller_template as controller_template
import random
import math
import os
import numpy
#from itertools import imap
################### váriaveis globais################
_changes = 10
#############################################

class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)
    
    def take_action(self, theta: list) -> int:
        """
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right
        2 - Left
        3 - Accelerate
        4 - Brake
        5 - Nothing
        """
        features = self.compute_features(self.sensors)
        f = numpy.array(features)
        t = numpy.split(numpy.array(theta), 5)

        R = numpy.dot(f, t[0])
        L = numpy.dot(f, t[1])
        A = numpy.dot(f, t[2])
        B = numpy.dot(f, t[3])
        N = numpy.dot(f, t[4])
        
        return [R,L,A,B,N].index(max([R,L,A,B,N])) + 1
    
    def compute_features(self, sensors):
        """
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):
            track_distance_left: 1-100
            track_distance_center: 1-100
            track_distance_right: 1-100
            on_track: 0 or 1
            checkpoint_distance: 0-???
            car_velocity: 10-200
            enemy_distance: -1 or 0-???
            position_angle: -180 to 180
            enemy_detected: 0 or 1

          (see the specification file/manual for more details)
        :return: A list containing the features you defined
        """
        features = []
        
        on_track = sensors[3]
        delta_left_right = sensors[0] - sensors[2]
        if delta_left_right > 30 or delta_left_right < -30:
            track_distance_left_right = 2*(on_track*delta_left_right+99)/198-1
        else:
            track_distance_left_right = 0
        track_distance_center = 2*(on_track*sensors[1]-1)/99-1
        #track_distance_left = 2*(on_track*sensors[0]-1)/99-1
        #track_distance_right = 2*(on_track*sensors[2]-1)/99-1
        
        #checkpoint_distance = 2*sensors[4]/999-1
        
        car_velocity = 2*(sensors[5]-10)/190-1
    
        if (sensors[8] == 1):
            enemy_distance = 2*sensors[6]/999-1
        else:
            enemy_distance = -1
        enemy_angle = 2*(sensors[7]+180/360)-1
        
        features.append(1.0)
        features.append(track_distance_left_right)
        features.append(track_distance_center)
        #features.append(track_distance_left)
        #features.append(track_distance_right)
        #features.append(checkpoint_distance)
        features.append(car_velocity)
        features.append(enemy_distance)
        features.append(enemy_angle)
        
        return features

    def learn(self, weights) -> list:
        def best_neighbor(state, state_score):
            current_score = state_score
            neighbor = state
            score = -999999        
            acc = 0
            while (score <= current_score):
                acc += 1
                if (acc > 50):
                    return -1, -1
                neighbor = state
                for i in range(_changes):
                    neighbor[random.randint(0, len(neighbor)-1)] += random.uniform(-.5, .5)
                score = self.run_episode(neighbor) 
            
            return neighbor, score
        
        def hill_climbing():
            current = weights
            current_score = self.run_episode(current)
            acc = 0

            while (True):
                print ("%d\t%f"%(self.episode-1, current_score))
                neighbor, neighbor_score = best_neighbor(current, current_score)             
                if (neighbor == -1):
                    return current, current_score
                current = neighbor
                current_score = neighbor_score
        
        while (True):
            state, score = hill_climbing()
            if (score > 9000):
                return state                

