import controller_template as controller_template
import random
import math
import os
import numpy
#from itertools import imap
################### váriaveis globais################
NUM_VIZINHOS = 40
VARIACAO_PERTUBACAO = 1
PRINTAR = True
PRINTAR_PESOS = False
#############################################

class Controller(controller_template.Controller):
	def __init__(self, track, evaluate=True, bot_type=None):
		super().__init__(track, evaluate=evaluate, bot_type=bot_type)



    #######################################################################
    ##### METHODS YOU NEED TO IMPLEMENT ###################################
    #######################################################################
	def take_action(self, theta: list) -> int:
		"""  
		:param parameters: Current weights/parameters of your controller
		:return: An integer corresponding to an action:
		1 - Right 
		2 - Left
		3 - Accelerate 
		4 - Brake
		5 - Nothing
		"""
		features = self.compute_features(self.sensors)
		f1 = features[0]
		f2 = features[1]
		f3 = features[2]
		R = theta[0]*f1 + theta[1]*f2 + theta[2]*f3
		L = theta[3]*f1 + theta[4]*f2 + theta[5]*f3
		A = theta[6]*f1 + theta[7]*f2 + theta[8]*f3
		B = theta[9]*f1 + theta[10]*f2 + theta[11]*f3
		N = theta[12]*f1 + theta[13]*f2 + theta[14]*f3
		maior = max([R,L,A,B,N])
		if maior == R: return 1
		if maior == L: return 2
		if maior == A: return 3
		if maior == B: return 4
		if maior == N: return 5

		raise NotImplementedError("This Method Must Be Implemented")


	def compute_features(self, sensors):
		"""        
		:param sensors: Car sensors at the current state s_t of the race/game
		contains (in order):        
		    track_distance_left: 1-100                      
		    track_distance_center: 1-100                    
		    track_distance_right: 1-100                     
		    on_track: 0 or 1                                
		    checkpoint_distance: 0-???                      
		    car_velocity: 10-200                            
		    enemy_distance: -1 or 0-???                     
		    position_angle: -180 to 180                     
		    enemy_detected: 0 or 1  

		  (see the specification file/manual for more details)
		:return: A list containing the features you defined 
		"""
		# def reset_episode():
		# 	self.ultimo_tic_checkpoint_distance = 10000000
		delta_maximum_value = 40

		track_distance_left = sensors[0]
		track_distance_center = sensors[1]
		track_distance_right = sensors[2]
		car_velocity = sensors[5]
		enemy_distance = sensors[6]
		enemy_angle = sensors[7]
		enemy_nerby = sensors[8]

		delta = track_distance_left - track_distance_right
		
		# if track_distance_right< 30 and track_distance_center < 80:
		# 	f1 = track_distance_left/99
		# elif track_distance_left < 30 and track_distance_center < 80:
		# 	f1 = -track_distance_right/99
		# else:
		f1 = delta/99
		f2 = track_distance_center/99

		if enemy_nerby == 1 and enemy_angle > -75 and enemy_angle < 75 and enemy_nerby < 50:
			f3 = enemy_distance/49
		else:
			f3 = 0

		return [f1,f2,f3]
		raise NotImplementedError("This Method Must Be Implemented")
	

	def pertubacao(self,constast_pertubacao):
		VARIACAO_PERTUBACAO = 2
		VARIACAO_PERTUBACAO /= pow(2,constast_pertubacao)
		OPCAO = 0
		if OPCAO == 1:
			return VARIACAO_PERTUBACAO
		else:
			return random.uniform( 0 , VARIACAO_PERTUBACAO)

	def hillClimbing(self,weights):
		old_weight_score = self.run_episode(weights)
		print("Score dos pesos originais: " + str(math.ceil(old_weight_score)))
		best_neighbor_score = old_weight_score
		neighbors = []
		constast_pertubacao = 0	
		while True:
			for i in range(0,NUM_VIZINHOS):
######################################## Generates a list of neighbors ##############################################
				list_new_weights = list(weights)																	#
				if(i<len(weights)):																					#
					list_new_weights[i] += self.pertubacao(constast_pertubacao)														#
				elif(i<(len(weights)*2)):																		#
					list_new_weights[i-len(weights)] -= self.pertubacao(constast_pertubacao)														#
				else:																								#
					list(map(lambda theta: random.uniform(-self.pertubacao(constast_pertubacao), self.pertubacao(constast_pertubacao))+ theta, weights))	#
#####################################################################################################################
				this_neighbor_score = self.run_episode(list_new_weights)

				if this_neighbor_score > best_neighbor_score:
					best_neighbor_score = this_neighbor_score
					weights = list(list_new_weights)
					print("Score do novo melhor score: " + str(math.ceil(best_neighbor_score*100)/100)+" Mudou o theta: "+ str(i))
					constast_pertubacao = 0
			if old_weight_score == best_neighbor_score:
				constast_pertubacao += 1
				if constast_pertubacao > 5:
################################### just saving the results #############################					
					if not os.path.exists("./params"):									#
						os.makedirs("./params")											#
					output = "params/"+str(math.ceil(old_weight_score))+".txt"			#
					numpy.savetxt(output, weights)										#
#########################################################################################
					return weights
			else:
				old_weight_score = best_neighbor_score
				print("Mais uma iteração do loop!!")



	def feixe_local(self,weights):
		fake_sensors = [53, 66, 100, 1, 172.1353274581511, 150, -1, 0, 0]
		features_len = len(self.compute_features(fake_sensors))
		weights2 = [random.uniform(-1, 1) for i in range(0, features_len*5)]
		weights3 = [random.uniform(-1, 1) for i in range(0, features_len*5)]
		weights4 = [random.uniform(-1, 1) for i in range(0, features_len*5)]
		weights5 = [random.uniform(-1, 1) for i in range(0, features_len*5)]
		
		print("Fazendo o hill climbing do primeiro: ")
		weights = self.hillClimbing(weights)
		print("Fazendo o hill climbing do segundo: ")
		weights2 = self.hillClimbing(weights2)
		print("Fazendo o hill climbing do terceiro: ")
		weights3 = self.hillClimbing(weights3)
		print("Fazendo o hill climbing do quarto: ")
		weights4 = self.hillClimbing(weights4)
		print("Fazendo o hill climbing do quinto: ")
		weights5 = self.hillClimbing(weights5)

		score_weights = self.run_episode(weights)
		score_weights2 = self.run_episode(weights2)
		score_weights3 = self.run_episode(weights3)
		score_weights4 = self.run_episode(weights4)
		score_weights5 = self.run_episode(weights5)

		best_score = max([score_weights5,score_weights4,score_weights3,score_weights2,score_weights])
		if best_score == score_weights: return weights
		if best_score == score_weights2: return weights2
		if best_score == score_weights3: return weights3
		if best_score == score_weights4: return weights4
		if best_score == score_weights5: return weights5

	# def generi	








	def learn(self, weights) -> list:
			
		

		return self.feixe_local(weights)



		raise NotImplementedError("This Method Must Be Implemented")
