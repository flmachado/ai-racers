import controller_template as controller_template
import random
import math
import os
import numpy
#from itertools import imap
################### váriaveis globais################
NUM_VIZINHOS = 60
VARIACAO_PERTUBACAO = 1
PRINTAR = True
PRINTAR_PESOS = False

_popSize = 25
_iterations = 50
_mutation = 0.15

#############################################

class Controller(controller_template.Controller):
	def __init__(self, track, evaluate=True, bot_type=None):
		super().__init__(track, evaluate=evaluate, bot_type=bot_type)



	#######################################################################
	##### METHODS YOU NEED TO IMPLEMENT ###################################
	#######################################################################

	def take_action(self, theta: list) -> int:
		features = self.compute_features(self.sensors)
		f = numpy.array(features)
		t = numpy.split(numpy.array(theta), 5)

		R = numpy.dot(f, t[0])
		L = numpy.dot(f, t[1])
		A = numpy.dot(f, t[2])
		B = numpy.dot(f, t[3])
		N = numpy.dot(f, t[4])

		return [R,L,A,B,N].index(max([R,L,A,B,N])) + 1


	def compute_features(self, sensors):
		"""        
		:param sensors: Car sensors at the current state s_t of the race/game
		contains (in order):        
		    track_distance_left: 1-100                      
		    track_distance_center: 1-100                    
		    track_distance_right: 1-100                     
		    #on_track: 0 or 1                                
		    #checkpoint_distance: 0-???                      
		    car_velocity: 10-200                            
		    enemy_distance: -1 or 0-???                     
		    position_angle: -180 to 180                     
		    enemy_detected: 0 or 1  

		  (see the specification file/manual for more details)
		:return: A list containing the features you defined 
		"""

		
		track_distance_left = sensors[0]
		track_distance_center = sensors[1]
		track_distance_right = sensors[2]
		car_velocity = sensors[5]
		enemy_distance = sensors[6]
		enemy_angle = sensors[7]
		enemy_nerby = sensors[8]

		delta = track_distance_left - track_distance_right

		f1 = 2*(delta/100)-1
		f2 = 2*((car_velocity-10)/190)-1

		if enemy_nerby == 1 and enemy_angle > -75 and enemy_angle < 75 and enemy_distance < 50:
			f3 = 2*((enemy_distance)/100) -1
		else:
			f3 = 0

		f4 = 2*((track_distance_center -1 )/(99))-1

		if enemy_nerby == 1: 
			f5 = 2*(enemy_nerby+180/360)-1 
		else:
			f5 = 0
		return [1,f1,f2,f3,f4,f5]
		
	def pertubacao(self,constast_pertubacao):
		VARIACAO_PERTUBACAO = 3
		VARIACAO_PERTUBACAO /= pow(2,constast_pertubacao)
		OPCAO = 1
		if OPCAO == 1:
			return VARIACAO_PERTUBACAO
		else:
			return random.uniform( 0 , VARIACAO_PERTUBACAO)

	def hillClimbing(self,weights):
		old_weight_score = self.run_episode(weights)
		print("Score dos pesos originais: " + str(math.ceil(old_weight_score)))
		best_neighbor_score = old_weight_score
		neighbors = []
		constast_pertubacao = 0	
		while True:
			for i in range(0,NUM_VIZINHOS):
######################################## Generates a list of neighbors ######################################################################################
				list_new_weights = list(weights)																											#
				if(i<len(weights)):																															#
					list_new_weights[i] += self.pertubacao(constast_pertubacao)																				#
				elif(i<(len(weights)*2)):																													#
					list_new_weights[i-len(weights)] -= self.pertubacao(constast_pertubacao)																#
				else:																																		#
					list(map(lambda theta: random.uniform(-self.pertubacao(constast_pertubacao), self.pertubacao(constast_pertubacao))+ theta, weights))	#
#############################################################################################################################################################
				this_neighbor_score = self.run_episode(list_new_weights)
				# print("Score vizinho atual: " + str(math.ceil(this_neighbor_score*100)/100))
				if this_neighbor_score > best_neighbor_score:
					best_neighbor_score = this_neighbor_score
					weights = list(list_new_weights)
					
					print("Score do novo melhor score: " + str(math.ceil(best_neighbor_score*100)/100)+" Mudou o theta: "+ str(i))
			if old_weight_score == best_neighbor_score:
				constast_pertubacao += 1
				print ("Iteração sem mudança: " + str(constast_pertubacao))
				if constast_pertubacao > 1:
################################### just saving the results #############################					
					if not os.path.exists("./params"):									#
						os.makedirs("./params")											#
					output = "params/"+str(math.ceil(old_weight_score))+".txt"			#
					numpy.savetxt(output, weights)										#
#########################################################################################
					return weights
			else:
				#constast_pertubacao = 0
				old_weight_score = best_neighbor_score
				print("Mais uma iteração do loop!!")



	def feixe_local(self,weights):
		fake_sensors = [53, 66, 100, 1, 172.1353274581511, 150, -1, 0, 0]
		features_len = len(self.compute_features(fake_sensors))
		weights2 = [random.uniform(-1, 1) for i in range(0, features_len*5)]
		weights3 = [random.uniform(-1, 1) for i in range(0, features_len*5)]
		weights4 = [random.uniform(-1, 1) for i in range(0, features_len*5)]
		weights5 = [random.uniform(-1, 1) for i in range(0, features_len*5)]
		
		print("Fazendo o hill climbing do primeiro: ")
		weights = self.hillClimbing(weights)
		print("Fazendo o hill climbing do segundo: ")
		weights2 = self.hillClimbing(weights2)
		print("Fazendo o hill climbing do terceiro: ")
		weights3 = self.hillClimbing(weights3)
		print("Fazendo o hill climbing do quarto: ")
		weights4 = self.hillClimbing(weights4)
		print("Fazendo o hill climbing do quinto: ")
		weights5 = self.hillClimbing(weights5)

		score_weights = self.run_episode(weights)
		score_weights2 = self.run_episode(weights2)
		score_weights3 = self.run_episode(weights3)
		score_weights4 = self.run_episode(weights4)
		score_weights5 = self.run_episode(weights5)

		best_score = max([score_weights5,score_weights4,score_weights3,score_weights2,score_weights])
		if best_score == score_weights: return weights
		if best_score == score_weights2: return weights2
		if best_score == score_weights3: return weights3
		if best_score == score_weights4: return weights4
		if best_score == score_weights5: return weights5

	def alg_gen(self,weights) -> list:
		def first_gen():
			features_len = len(self.compute_features([1,1,1,0,0,10,0,0,0]))
			new_population = []
			for i in range(_popSize):  #N is the size of population
				new_individual = [random.uniform(-1,1) for i in range(features_len*5)]
				new_population.append(new_individual)
			return new_population

		def genetic_algorithm(population):
			def fitness(population):
				fit_values = []
				for i in range(len(population)):
					fit_values.append(self.run_episode(population[i]))
		        
				f = numpy.array(fit_values)
				population_sum = numpy.sum(fit_values)
				lowest = numpy.absolute(numpy.amin(fit_values))
				population_sum += _popSize*lowest
				f = (f + lowest)/population_sum

				return f.tolist()

			def best_fit(population, fit_values):
				return population[fit_values.index(max(fit_values))]

			def select_parent(population, fit_values):
				i = 0
				acc = 0.0
				rn = random.random()
				while ((rn - acc) > 0.01):
					acc += fit_values[i]
					i += 1

				if (rn > 0.0):
					return population[i-1]
				else:
					return population[i]


			def reproduce(parent1, parent2):
				crossover_point = random.randint(0,len(parent1)-1)

				child = parent1[:crossover_point]
				child.extend(parent2[crossover_point:])

				return child

			def mutate(child):
				index = random.randint(0,len(child)-1)
				child[index] = random.uniform(-1,1)

				return child
		    
			fit_values = fitness(population)
			for i in range(_iterations):
				print ("generation #%d"%i)
				new_population = []
				for j in range(len(population)):
					parent1 = select_parent(population, fit_values)
					parent2 = select_parent(population, fit_values)
					child = reproduce(parent1, parent2)
					if (random.random() < _mutation): # chance of a mutation occur
						child = mutate(child);
					new_population.append(child)

				population = new_population
				fit_values = fitness(population)

			return best_fit(population, fit_values)

		new_population = first_gen()
		return genetic_algorithm(new_population)

	def ajuste(self,weights):
		old_weight_score = self.run_episode(weights)
		print("Score dos pesos originais: " + str(math.ceil(old_weight_score)))
		best_neighbor_score = old_weight_score
		neighbors = []
		constast_pertubacao = 0	
		for i in range(0,NUM_VIZINHOS):
######################################## Generates a list of neighbors ##################################################################################
			list_new_weights = list(weights)																											#
			if(i<len(weights)):																															#
				list_new_weights[i] += self.pertubacao(constast_pertubacao)																				#
			elif(i<(len(weights)*2)):																													#
				list_new_weights[i-len(weights)] -= self.pertubacao(constast_pertubacao)																#
			else:																																		#	
				list(map(lambda theta: random.uniform(-self.pertubacao(constast_pertubacao), self.pertubacao(constast_pertubacao))+ theta, weights))	#
#########################################################################################################################################################
			this_neighbor_score = self.run_episode(list_new_weights)
			# print("Score vizinho atual: " + str(math.ceil(this_neighbor_score*100)/100))
			if this_neighbor_score > best_neighbor_score:
				best_neighbor_score = this_neighbor_score
				weights = list(list_new_weights)
				print("Score do novo melhor score: " + str(math.ceil(best_neighbor_score*100)/100)+" Mudou o theta: "+ str(i))		
		
################################### just saving the results #############################					
		if not os.path.exists("./params"):												#
			os.makedirs("./params")														#
		output = "params/"+str(math.ceil(old_weight_score))+".txt"						#
		numpy.savetxt(output, weights)													#
#########################################################################################
		return weights
	
    def learn(self, weights) -> list:
			
		

		return self.hillClimbing(weights)



		raise NotImplementedError("This Method Must Be Implemented")

        

