import controller_template as controller_template
import random
import math
import os
import numpy
#from itertools import imap
################### váriaveis globais################
_popSize = 50
_iterations = 50
_mutation = 0.15
#############################################

class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)



    #######################################################################
    ##### METHODS YOU NEED TO IMPLEMENT ###################################
    #######################################################################
    def take_action(self, theta: list) -> int:
        """
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right
        2 - Left
        3 - Accelerate
        4 - Brake
        5 - Nothing
        """
        features = self.compute_features(self.sensors)
        f = numpy.array(features)
        t = numpy.split(numpy.array(theta), 5)

        R = numpy.dot(f, t[0])
        L = numpy.dot(f, t[1])
        A = numpy.dot(f, t[2])
        B = numpy.dot(f, t[3])
        N = numpy.dot(f, t[4])
        
        return [R,L,A,B,N].index(max([R,L,A,B,N])) + 1
    
    def compute_features(self, sensors):
        """
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):
            track_distance_left: 1-100
            track_distance_center: 1-100
            track_distance_right: 1-100
            on_track: 0 or 1
            checkpoint_distance: 0-???
            car_velocity: 10-200
            enemy_distance: -1 or 0-???
            position_angle: -180 to 180
            enemy_detected: 0 or 1

          (see the specification file/manual for more details)
        :return: A list containing the features you defined
        """
        features = []
        
        on_track = sensors[3]
        delta_left_right = sensors[0] - sensors[2]
        if delta_left_right > 30 or delta_left_right < -30:
            track_distance_left_right = 2*(on_track*delta_left_right+99)/198-1
        else:
            track_distance_left_right = 0
        track_distance_center = 2*(on_track*sensors[1]-1)/99-1
        #track_distance_left = 2*(on_track*sensors[0]-1)/99-1
        #track_distance_right = 2*(on_track*sensors[2]-1)/99-1
        
        #checkpoint_distance = 2*sensors[4]/999-1
        
        car_velocity = 2*(sensors[5]-10)/190-1
    
        if (sensors[8] == 1):
            enemy_distance = 2*sensors[6]/999-1
        else:
            enemy_distance = -1
        enemy_angle = 2*(sensors[7]+180/360)-1
        
        features.append(1.0)
        features.append(track_distance_left_right)
        features.append(track_distance_center)
        #features.append(track_distance_left)
        #features.append(track_distance_right)
        #features.append(checkpoint_distance)
        features.append(car_velocity)
        features.append(enemy_distance)
        features.append(enemy_angle)
        
        return features

    def learn(self, weights) -> list:
        def first_gen():
            features_len = len(self.compute_features([1,1,1,0,0,10,0,0,0]))
            new_population = []
            for i in range(_popSize):  #N is the size of population
                new_individual = [random.uniform(-1,1) for i in range(features_len*5)]
                new_population.append(new_individual)
            return new_population

        def genetic_algorithm(population):
            def fitness(population):
                fit_values = []
                for i in range(len(population)):
                    fit_values.append(self.run_episode(population[i]))
                f = numpy.array(fit_values)
                print ("%.2f"%numpy.average(f))
                population_sum = numpy.sum(fit_values)
                lowest = numpy.absolute(numpy.amin(fit_values))
                population_sum += _popSize*lowest
                f = (f + lowest)/population_sum

                return f.tolist()

            def best_fit(population, fit_values):
                return population[fit_values.index(max(fit_values))]

            def select_parent(population, fit_values):
                i = 0
                acc = 0.0
                rn = random.random()
                while ((rn - acc) > 0.01):
                    acc += fit_values[i]
                    i += 1

                if (rn > 0.0):
                    return population[i-1]
                else:
                    return population[i]


            def reproduce(parent1, parent2):
                crossover_point = random.randint(0,len(parent1)-1)

                child = parent1[:crossover_point]
                child.extend(parent2[crossover_point:])

                return child

            def mutate(child):
                index = random.randint(0,len(child)-1)
                child[index] = random.uniform(-1,1)

                return child

            print ("0\t", end="")
            fit_values = fitness(population)
            for i in range(_iterations):
                print ("%d\t"%(i+1), end="")
                new_population = []
                for j in range(len(population)):
                    parent1 = select_parent(population, fit_values)
                    parent2 = select_parent(population, fit_values)
                    child = reproduce(parent1, parent2)
                    if (random.random() < _mutation): # chance of a mutation occur
                        child = mutate(child);
                    new_population.append(child)

                population = new_population
                fit_values = fitness(population)

            return best_fit(population, fit_values)
        print ("#population=%d episodes=%d mutation=%.2f"%(_popSize, _iterations*_popSize, _mutation))
        new_population = first_gen()
        return genetic_algorithm(new_population)

